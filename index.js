var gulp = require('gulp');
var git = require('gulp-git');
var gutil = require('gulp-util');
var bump = require('gulp-bump');
var runSequence = require('run-sequence');
var fs = require('fs');

var username = null;
var password = null;
var repositoryUrl = null;

module.exports = function(opts) {
    username = opts.username;
    password = opts.password;
    repositoryUrl = opts.repositoryUrl;

    runSequence(
        'checkout-master',
        'merge-develop',
        'tag-and-push-master',
        'checkout-develop',
        'bump-version',
        'push-develop',
        'checkout-master',
        function (error) {
            if (error) {
                console.log(error.message);
            } else {
                console.log('RELEASE TRIGGERED SUCCESSFULLY');
            }
        });
}

gulp.task('merge-develop', function (callback) {
    git.merge('origin/develop', callback);
});

gulp.task('tag-and-push-master', function (callback) {
    var version = JSON.parse(fs.readFileSync('./package.json', 'utf8')).version;

    git.tag(version, 'Created Tag for version: ' + version, function (error) {
        if (error) {
            return callback(error);
        }
        git.push(repositoryUrl, 'master', {args: '--tags'}, callback);
    });
});

gulp.task('checkout-develop', function (callback) {
    git.checkout('develop', callback);
});

gulp.task('bump-version', function () {
    return gulp.src('./package.json')
        .pipe(bump({ type: 'minor' })
            .on('error', gutil.log))
        .pipe(gulp.dest('./'))
        .pipe(git.add())
        .pipe(git.commit('[Post Release] Bumped version number'));
});

gulp.task('push-develop', function (callback) {
    git.push(repositoryUrl, 'develop', callback);
});

gulp.task('checkout-master', function (callback) {
    git.checkout('master', callback);
});
