# Gulp ABA Git Release

Plugin for gulp for carrying out releases of an npm managed project that uses git as its version control system.

The steps it performs are:

1. Check out master branch
2. Merge a branch named 'develop' to master (current branch)
3. Tag the branch using the version number found in the project's package.json
4. Push new tag and merge result to master
5. Check out branch named 'develop'
6. Bump minor version number found in the project's package json, thus preparing it for next release
7. Push change to minor version to develop
8. Checkout master branch again
